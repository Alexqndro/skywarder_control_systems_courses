# Skywarder_control_systems_courses

## Description
Here my courses for the control systems team.
All codes were developed as part of educational activities, so they do not necessarily reflect the actual work done by the Skywarder Association.

## Course I: Parafoil modeling and MPC
In this course, control oriented models of a parafoil are introduced both in matlab and simulink, then these model are linearized around equilibrium positions and used to design a PID controller and a non-linear MPC controller.
### Usage
Simulate the parafoil dynamics: Run "main_parafoil_dynamics.mlx"
Control the parafoil with a nMPC: Run "main_parafoil_dynamics.mlx"

## Course II: Temperature control lab overview
In this course, the arduino temperature control lab is presented and used to describe all the phases of the implementation of a control system: 
- First principle modeling and simulation
- Linear analysis
- System identification
- Filtering
- Control system

### Usage
Simulate the dynamics and linear analysis: Run "TCLab_Modelling.mlx"
System identification, kalman and control: Run "TCLab_Identification.mlx"

## Support & Contributing
For any questions and suggestions, or if you want to contribute, write to alessandrodelduca.96@gmail.com

## Authors and acknowledgment
Author: Alessandro Del Duca

## License
All the code in the repository is under GNU General public License 3.0v 

## Project status
Running