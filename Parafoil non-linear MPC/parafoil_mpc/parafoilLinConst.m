function [A,b,C,d] = parafoilLinConst(umin, umax, N)

d  = [umin*ones(N,1);-umax*ones(N,1)];
C  = zeros(length(d),200);
for i=1:N
    C(2*i-1:2*i,2*i-1:2*i) = eye(2);
end
C(N+1:2*N,1:N) = -C(1:N,1:N);

A = [];
b = [];
end