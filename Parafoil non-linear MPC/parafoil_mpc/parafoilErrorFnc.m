function  [v,h] = parafoilErrorFnc(t, x, xio, yref, p)

U     = x(1:length(t));
Xi    = x(41:end);
dt    = t(2)-t(1);
pfoil = @(t,xi)parafoil4dof(t, xi, U, p);

xi       = zeros(4,length(t));
xi(:,1)  = xio;

for ii = 2:length(t)
%     [~, xitemp]      = ode45(pfoil, t(ii-1:ii), xi(:,ii-1), [U(ii-1);0]);
%     xi(:,ii)         = xitemp(end,:);
    dxi = parafoil4dof(t,xi(:,ii-1), [U(ii-1);0],p);
    xi(:,ii) = xi(:,ii-1) + dt*dxi;
end

for ii = 1:length(t)
    h((4*ii-3):4*ii,1) = xi(:,ii) - Xi((4*ii-3):4*ii);
end

error  = sqrt(p.Q)*(yref - xi(2,:))';
energy = sqrt(p.R)*(U(:));
F      = [error; energy];
v      = [F;h];
end
