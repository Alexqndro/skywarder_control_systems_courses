function dx = parafoil4dof(t, x, U, p, varargin)

% States of the system
phi = x(1);
psi = x(2);
u   = x(3);
w   = x(4);


% Input of the system
da  = U(1);
ds  = U(2);

% Airspeed
Va = sqrt(u^2 + w^2);
% Drag and lift wind frame
D = 0.5 * p.rho * Va^2 * p.S * (p.Cdo + p.Cd_ds * ds);
L = 0.5 * p.rho * Va^2 * p.S * (p.Clo + p.Cl_ds * ds);
% Drag and lift  inertial frame
alfa   = atan2(w,u);

phi_dot = 1/p.T_phi * ( - phi + p.K_phi * da);
psi_dot = p.g/u * tan(phi) + w * phi_dot / (u * cos(phi));
u_dot   = (L * sin(alfa) - D * cos(alfa))/p.m - w * psi_dot * sin(phi);
w_dot   = (-L * cos(alfa) - D * sin(alfa))/p.m + p.g * cos(phi) - u * psi_dot * sin(phi);

dx(1) = phi_dot;
dx(2) = psi_dot;
dx(3) = u_dot;
dx(4) = w_dot;
dx    = dx';

end


