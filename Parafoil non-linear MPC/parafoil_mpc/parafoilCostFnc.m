function V = parafoilCostFnc(t, x, xio, yref, p)

[v,h] = parafoilErrorFnc(t, x, xio, yref, p);
F = v(1:end-length(h));
J = F'*F;
V = [J;h];
end