function p = parafoilParam 

% Drag and Lift coeff
p.Cdo   = 0.173;
p.Clo   = 0.502;
p.Cd_ds = 1.086;
p.Cl_ds = 0.892;

% Atmosphere 
p.rho   = 1.225;
p.g     = 9.81;

% Roll dynamic
p.K_phi = 0.504;
p.T_phi = 0.994;

% Mass and geometry
p.m     = 122;
p.S     = 23.36;

% Initial conditions
phio = 0;    psio = 0; 
uo   = 10;    vo   = 0;     wo = 5;
Xo   = -100; Yo   = -100;  Zo = -1450;

p.xo    = [phio; psio; Xo; Yo; Zo; uo; vo; wo];
p.xolin = [phio; psio; uo; wo];

% MPC parameters
p.N     = 100;
p.R     = 0.01;
p.Q     = 10;

end

