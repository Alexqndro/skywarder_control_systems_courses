function dx = parafoil(t, x, U, p, varargin)

% States of the system
phi = x(1);
psi = x(2);
X   = x(3);
Y   = x(4);
Z   = x(5);
u   = x(6);
v   = x(7);
w   = x(8);


Rpsi = [   cos(psi) sin(psi)   0; 
          -sin(psi) cos(psi)   0; 
               0        0      1;];
Rphi = [   1       0        0; 
           0   cos(phi) sin(phi); 
           0  -sin(phi) cos(phi)];
Vels= Rpsi*Rphi*[u; v; w];

% Input of the system
da  = U(1);
ds  = U(2);

% Airspeed
Va = sqrt(u^2 + w^2);
% Drag and lift wind frame
D = 0.5 * p.rho * Va^2 * p.S * (p.Cdo + p.Cd_ds * ds);
L = 0.5 * p.rho * Va^2 * p.S * (p.Clo + p.Cl_ds * ds);
% Drag and lift  inertial frame
alfa   = atan2(w,u);

phi_dot = 1/p.T_phi * ( - phi + p.K_phi * da);
psi_dot = p.g/u * tan(phi) + w * phi_dot / (u * cos(phi));
dX      = u;
dZ      = w;
u_dot   = (L * sin(alfa) - D * cos(alfa))/p.m - w * psi_dot * sin(phi);
w_dot   = (-L * cos(alfa) - D * sin(alfa))/p.m + p.g * cos(phi) - u * psi_dot * sin(phi);

dx(1) = phi_dot;
dx(2) = psi_dot;
dx(3) = Vels(1);
dx(4) = Vels(2);
dx(5) = Vels(3);
dx(6) = u_dot;
dx(7) = 0;
dx(8) = w_dot;
dx    = dx';


end


