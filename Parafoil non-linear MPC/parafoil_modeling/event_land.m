function  [value, isterminal, direction] = event_land(t, x, varargin)
value  = - x(5);

if value > 0
    isterminal = 0;
else
    isterminal = 1;
end
direction = 0;
end
