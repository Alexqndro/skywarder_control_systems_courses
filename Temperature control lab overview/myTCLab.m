function dT = myTCLab(t, T, Q, p)

T1  = T(1);
T2  = T(2);
Q1  = Q(:,1);
Q2  = Q(:,2);
dT1 = p.alfa*(p.Ta - T1) + p.beta*(p.Ta^4 - T1^4) + p.alfa12*(T2 - T1) + p.beta12*(T2^4 - T1^4) + p.gamma1*Q1;

dT2 = p.alfa*(p.Ta - T2) + p.beta*(p.Ta^4 - T2^4) - p.alfa12*(T2 - T1) - p.beta12*(T2^4 - T1^4) + p.gamma2*Q2;

dT  = [dT1; dT2];
end
