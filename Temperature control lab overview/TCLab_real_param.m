function p = TCLab_real_param
%% Parameters
% Ambient temperature
p.Ta = 23 + 273.15;
% Initial temperature 
p.T0 = 23 + 273.15;

% Heater 1 
Q1   = 1;               % W
p.c1 = Q1/100;          % W/(heater input, 0-100)

% Heater 2
Q2   = 0.75;            % W
p.c2 = Q2/100;          % W/(heater input, 0-100)

% Areas
p.A  = 11 / 100^2;      % m^2
p.As = 2.5 / 100^2;     % m^2

% Mass
p.m  = 5.5 / 1000;      % kg (4 gm)

% Heat transfer coefficient
p.U = 12;               % W/m^2-K

% Heat capacity
p.Cp = 500.0;           % J/kg-K

% Stefan-Boltzmann constant
p.sigma = 5.67e-8;      % W/m^2-K^4

% Emissivity
p.eps = 0.9;            % emisivity


p.alfa   = 1/(p.m*p.Cp)*(p.U*p.A);              % T1 - Tinf
p.beta   = 1/(p.m*p.Cp)*(p.eps*p.sigma*p.A);    % T1^4 - Tinf^4, T2^4 - Tinf^4
p.alfa12 = 1/(p.m*p.Cp)*(p.U*p.As);             % T1 - T2, T2 - T1
p.beta12 = 1/(p.m*p.Cp)*(p.eps*p.sigma*p.As);   % T1^4 - T2^4, T2^4 - T1^4
p.gamma1 = 1/(p.m*p.Cp)*p.c1;                   % Q1  [%]
p.gamma2 = 1/(p.m*p.Cp)*p.c2;                   % Q2  [%]

end


% delta = alpha + 3 * beta * Ta^3;
% 
% % approximate gain
% gain = gamma / delta;
% 
% % approximate time constant
% tau = 1/delta;